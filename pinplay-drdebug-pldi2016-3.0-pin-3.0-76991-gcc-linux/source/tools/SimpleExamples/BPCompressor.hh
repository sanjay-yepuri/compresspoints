#ifndef __BP_COMPRESSOR_HH__
#define __BP_COMPRESSOR_HH__

#include "common.hh"

class BPSCompressor64 : public Compressor {
	public:
		BPSCompressor64(const string name, int diff, int bp, int code, int fragblocks)
			: Compressor(name), diff_mode(diff), bp_mode(bp), code_mode(code), frag_mode(fragblocks){}
		~BPSCompressor64() {}
	public:
		void reset() {
			Compressor::reset();

			prev_zero = true;
			prev_data = 0;
			prev_delta = 0;
			prev_line = {};

			run_length = 0;
			run_length_orig = 0;
		}

		CACHELINE_DATA* transform(CACHELINE_DATA* line, CACHELINE_DATA &buffer) {
			if (diff_mode==2) {       // XOR
				for (int i=0; i<_MAX_DWORDS_PER_LINE; i++) {
					buffer.dword[i] = (line->dword[i] ^ prev_data);
					prev_data = line->dword[i];
				}
			}
			return &buffer;
		}
		unsigned compressLine(CACHELINE_DATA* line, UINT64 line_addr) {
			CACHELINE_DATA diff_buffer;
			CACHELINE_DATA *diff_result = transform(line, diff_buffer);

			// BP mode
			//TODO: These sizes need to be changed for a smaller cache line size
			CACHELINE_DATA bp_buffer = {};
			CACHELINE_DATA dbp_buffer = {};
			CACHELINE_DATA dbx_buffer = {};
			CACHELINE_DATA dbx2_buffer = {};
			CACHELINE_DATA *bp_result = NULL;

			if (bp_mode==4) {    
				for (int j=31; j>=0; j--) {
					INT32 bufDBP = 0;
					INT32 bufDBX = 0;
					for (int i=_MAX_DWORDS_PER_LINE-1; i>=1; i--) {
						bufDBP  <<= 1;
						bufDBX  <<= 1;
						bufDBP  |= ((diff_result->dword[i]>>j)&1);
						if (j==31) {
							bufDBX  |= ((diff_result->dword[i]>>j)&1);
						} else {
							bufDBX  |= (((diff_result->dword[i]>>j)^(diff_result->dword[i]>>(j+1)))&1);
						}
					}
					dbp_buffer.word[j]  = bufDBP;
					dbx_buffer.word[j]  = bufDBX;
				}
				bp_result = &dbx_buffer;
			}
			unsigned blkLength = 0;
			if (code_mode==10) {
				blkLength = encode_paper(&dbx_buffer, &dbp_buffer, line);
			} 
			countLineResult(blkLength);

			return blkLength;
		}

		unsigned encode_paper(CACHELINE_DATA *dbx, CACHELINE_DATA *dbp, CACHELINE_DATA *line) {
			//static const unsigned ZRL_CODE_SIZE[33] = {0, 4, 8, 6, 8, 11, 7, 7, 9, 10, 9, 8, 9, 9, 10, 10, 10, 11, 9, 9, 10, 5, 8, 9, 10, 11, 11, 6, 9, 7, 10, 8, 10};

			//static const unsigned ZRL_CODE_SIZE[33] = {0, 4, 6, 7, 8, 9, 6, 10, 12, 12, 8, 8, 9, 10, 9, 11, 11, 9, 9, 9, 10, 11, 10, 9, 7, 8, 8, 5, 7, 11, 10, 11, 8};

			//static const unsigned ZRL_CODE_SIZE[17] = {0, 4, 6, 7, 8, 7, 6, 8, 8, 8, 8, 9, 9, 9, 9, 7, 5 };
			static const unsigned ZRL_CODE_SIZE[17] = {0, 3, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 };
			unsigned length = 0;
			run_length = 0;
			run_length_orig = 0;
			for (int i=_MAX_DWORDS_PER_LINE-1; i>=0; i--) {

				if (dbx->dword[i]==0) {
					run_length++;
				}
				else if((run_length==0) && (line->dword[i]==0)){
					run_length_orig++;
				}
				else {
					if ((run_length>0) || (run_length_orig>0)) {
						int run_len = run_length+run_length_orig;
						countPattern(run_len-1);
						length += ZRL_CODE_SIZE[run_len] + 1; //ESHA
					}
					run_length = 0;
					run_length_orig = 0;
					//ESHA
					if(line->dword[i]==0) {
						run_length_orig++;
					}
					else {
						if (dbp->dword[i]==0) {
							length += 5;
							countPattern(33);
						} else if (dbx->dword[i]==0xffffffff) {
							length += 5;
							countPattern(34);
						} else {
							int oneCnt = 0;
							for (int j=0; j<32; j++) {
								if ((dbx->dword[i]>>j)&1) {
									oneCnt++;
								}
							}
							unsigned two_distance = 0;
							int firstPos = -1;
							if (oneCnt<=2) {
								for (int j=0; j<32; j++) {
									if ((dbx->dword[i]>>j)&1) {
										if (firstPos==-1) {
											firstPos = j;
										} else {
											two_distance = j - firstPos;
										}
									}
								}
							}
							if (oneCnt==1) {
								length += 10;
								countPattern(64+firstPos);
							} else if ((oneCnt==2) && (two_distance==1)) {
								length += 10;
								countPattern(128+firstPos);
							} else {
								length += 32;
								countPattern(36);
							}
						}
					}
				}

			}
			if ((run_length>0)||(run_length_orig>0)) {
				int run_len = run_length+run_length_orig;
				length += ZRL_CODE_SIZE[run_len]+1;
				countPattern(run_len-1);
			}
			if(run_length==16 || run_length_orig==16){
				length = 0;	
			}
		//	cout << " " << length;
			return length;
		}
	protected:
		// zero run length counters
		int diff_mode;
		int bp_mode;
		int code_mode;
		int frag_mode;

		INT32 prev_data;
		INT32 prev_delta;
		CACHELINE_DATA prev_line;
		int run_length, run_length_orig;
		bool prev_zero;
};

#endif /* __BP_COMPRESSOR_HH__ */
