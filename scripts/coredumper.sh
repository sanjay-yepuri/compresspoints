#!/bin/bash

dump_process()
{
	pagesize=`getconf PAGESIZE`
	local pid="$1"
	local dir="$2"
	
	# save command line
	cp /proc/$pid/cmdline "$dir/"
	
	# directories for libs and user data
	mkdir -p "$dir/memory/shared-libs" "$dir/memory/user"
	
	# start parsing maps file
	cat $infile | while read line; do
		range=`echo $line | awk '{print $1;}'`
		perms=`echo $line | awk '{print $2;}'`
		what=`echo $line | awk '{print $6;}'`
		
		rangestart=`echo $range | cut -d- -f1`
		
		if test "${perms:0:1}" = "r"; then
			permbits="${perms:0:4}"
			case "$what" in
			"")
					dump_range "/proc/$pid/mem" "$range" "$dir/memory/user/$permbits-user@$rangestart"
				;;
			"[heap]")
				dump_range "/proc/$pid/mem" "$range" "$dir/memory/user/$permbits-heap@$rangestart"
				;;
			"[stack]")
				dump_range "/proc/$pid/mem" "$range" "$dir/memory/user/$permbits-stack@$rangestart"
				;;
			*pinplay*)
				;;
			*.so*)
				libname=`basename "$what"`
				dump_range "/proc/$pid/mem" "$range" "$dir/memory/shared-libs/$permbits-$libname@$rangestart"
				;;
			*)	
				what=`basename "$what"`
				dump_range "/proc/$pid/mem" "$range" "$dir/memory/user/$permbits-$what@$rangestart"
				;;
			esac
		fi
	done
}

dump_range()
{
	local image="$1"
	local range="$2"
	local outf="$3"
	
	rangestart=$(( 0x`echo $range | cut -d- -f1` ))
	rangeend=$(( 0x`echo $range | cut -d- -f2` ))
	
	rangesize=$(( $rangeend - $rangestart ))
	
	# sanity check
	if test $(( $rangestart % $pagesize )) -gt 0 -o \
		$(( $rangeend % $pagesize )) -gt 0 -o \
		$(( $rangesize % $pagesize )) -gt 0; then
		fail_with_error "pid $pid region at $rangestart is not a multiple of $pagesize"
	fi
	
	# divide everything by the page size
	rangestart=$(( $rangestart / $pagesize ))
	rangeend=$(( $rangeend / $pagesize ))
	rangesize=$(( $rangesize / $pagesize ))
	
	# dump!
	set +e
	dd if="$image" bs="$pagesize" skip="$rangestart" count="$rangesize" of="$outf" 2>/dev/null
	set -e
}

pid=`basename "$1"`
infile="$2"
outdir="$3"

i=0
successful=0
if ! test -d "/proc/$pid" ; then
echo "pid $pid has died"
continue
fi
mkdir -p "$outdir/"
ps u "$pid" > "$outdir/ps-u"
if dump_process "$pid" "$outdir/"; then
successful=$[successful+1]
else
rm -rf "$outdir/by-pid/$pid"
fi
echo -en "\r"

# clear whole line
echo -en "\e[2K"
echo "Process dumping completed "
