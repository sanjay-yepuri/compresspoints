#!/bin/bash

temp=$1
bench=$2
MEMDUMP_PATH=/home/esha/Documents/Research/Release_CompressPoints/memdump
count=0
mv $bench-user.out $bench-user.backup
rm temp*.txt
while true
do
	pids=`pgrep $bench`
	IFS=$'\n' read -r -a array <<< "$pids"
	length=${#array[@]}
	if [ $length == 0 ]
	then
		continue
	fi
	pid=${array[0]}
	echo $pid
	while [ ! -f $temp ]
	do
		sleep 2
	done
	ls -l $temp
	rm $temp
	kill -TSTP $pid
	echo $pid
	textT=$count"Checkpoint"
	echo $textT >> $bench-user.out
	echo $textT >> $bench-lib.out
	./pagemap $pid pagemapout
	./coredumper.sh $pid pagemapout $MEMDUMP_PATH/$bench
	./vsc $MEMDUMP_PATH/$bench/memory/user/* >> temp1.txt
	echo "Compress done"
	if [ -f temp2.txt ]
	then
		python calculate_overflows.py temp1.txt temp2.txt >> $bench/$bench-user.out	
	else
		python calculate_overflows.py temp1.txt >> $bench/$bench-user.out
	fi
	mv temp1.txt temp2.txt
	echo "Next"
	rm -rf $MEMDUMP_PATH/$bench/memory/
	kill -CONT $pid
	count=$(($count+1))
done
